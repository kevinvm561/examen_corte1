import java.io.Serializable;

public class recibonomina implements Serializable {

    private int numRecibo;
    private String nombre;
    private float horasTNormal;
    private float horasTExtra;
    private int puesto;
    private float impPor;
    private float sueldoE;

    private float sueldoN;

    public recibonomina(int numRecibo, String nombre, float horasTNormal, float horasTExtra, int puesto, float impPor) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTNormal = horasTNormal;
        this.horasTExtra = horasTExtra;
        this.puesto = puesto;
        this.impPor = impPor;
    }

    public recibonomina() {
    }


    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTNormal() {
        return horasTNormal;
    }

    public void setHorasTNormal(float horasTNormal) {
        this.horasTNormal = horasTNormal;
    }

    public float getHorasTExtra() {
        return horasTExtra;
    }

    public void setHorasTExtra(float horasTExtra) {
        this.horasTExtra = horasTExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpPor() {
        return impPor;
    }

    public void setImpPor(float impPor) {
        this.impPor = impPor;
    }

    public float getSueldoE() {
        return sueldoE;
    }

    public void setSueldoE(float sueldoE) {
        this.sueldoE = sueldoE;
    }

    public float getSueldoN() {
        return sueldoN;
    }

    public void setSueldoN(float sueldoN) {
        this.sueldoN = sueldoN;
    }

    public float CalcularSubtotal(){

        float subtotal=(horasTNormal*sueldoN) + (horasTNormal * sueldoE);
        return subtotal;
    }

    public float CalcularImpuestos(){
    float impuesto= (CalcularSubtotal() * impPor) / 100;
    return impuesto;
    }

    public float CalcularTotal(){
      float totalPagar= CalcularSubtotal() - CalcularImpuestos();
      return totalPagar;
    }
}
