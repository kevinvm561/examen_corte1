package com.example.examen_calculonomina;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

public class pagonominaActivity extends AppCompatActivity {

    private ReciboNomina recibonomina;
    private EditText  numeroRecibo ;

    private EditText  Nombre;
    private EditText   HorasTrabajadasNormal;
    private EditText   HorasTrabajadasExtra;
    private EditText   Subtotal;
    private EditText   impuestos;
    private EditText   TotalPagar;
    private RadioButton  rbAux;
    private RadioButton  rbAlb;
    private RadioButton  rbIng;

    private Button  btnCalcular;
    private Button  btnLimpiar;
    private Button  btnRegresar;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagonomina_view);

        Nombre=(EditText) findViewById(R.id.txtNombre);
        HorasTrabajadasExtra=(EditText)findViewById(R.id.txtHorasEx);
        HorasTrabajadasNormal=(EditText)findViewById(R.id.txtHorasT);
        Subtotal=(EditText)findViewById(R.id.txtSubtotal);
        impuestos=(EditText)findViewById(R.id.txtImpuesto);
        TotalPagar=(EditText)findViewById(R.id.txtTotal);
        rbAux=(RadioButton) findViewById(R.id.rbAux);
        rbAlb=(RadioButton) findViewById(R.id.rbAlb);
        rbIng=(RadioButton) findViewById(R.id.rbIng);

        btnCalcular=(Button) findViewById(R.id.btnCalcular);
        btnLimpiar=(Button) findViewById(R.id.btnLimpiar);
        btnRegresar=(Button) findViewById(R.id.btnRegresar);



        Bundle datos=getIntent().getExtras();

        numeroRecibo.setText("10");
        Nombre.setText(datos.getString("nombre"));
        recibonomina = (ReciboNomina)  datos.getSerializable("recibo");


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int puesto=0;
                if(rbAux.isChecked()){
                    puesto=1;
                }else if(rbAlb.isChecked()){
                    puesto=2;
                }else if(rbIng.isChecked()){
                    puesto=3;
                }



                recibonomina.setHorasTExtra(Float.valueOf(HorasTrabajadasExtra.getText().toString()));
                recibonomina.setHorasTNormal(Float.valueOf(HorasTrabajadasNormal.getText().toString()));
                recibonomina.setImpPor(Float.valueOf("16"));
                recibonomina.setPuesto(puesto);
                Subtotal.setText(String.valueOf(recibonomina.CalcularSubtotal()));
                impuestos.setText(String.valueOf(recibonomina.CalcularImpuestos()));
                TotalPagar.setText(String.valueOf(recibonomina.CalcularTotal()));
            }
        });

    }
}
