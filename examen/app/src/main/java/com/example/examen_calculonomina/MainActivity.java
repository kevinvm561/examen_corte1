package com.example.examen_calculonomina;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private  ReciboNomina recibo;
    private EditText txtnombre;
    private Button btnEntrar;

    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        txtnombre=(EditText) findViewById(R.id.txtnombreEmpleado);
        btnSalir=(Button) findViewById(R.id.btnSalir);
     recibo= new ReciboNomina();


     btnEntrar.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
        String nombre = txtnombre.getText().toString();
if(nombre.equals("") == true   ){
    Toast.makeText(MainActivity.this,"Ingresa el nombre antes de continuar", Toast.LENGTH_SHORT).show();

} else{
    Intent intent = new Intent(MainActivity.this,pagoNom.class);
    intent.putExtra("nombre",nombre);
     intent.putExtra("numero",String.valueOf(generarNumeroRecibo()));
    Bundle obj= new Bundle();
    obj.putSerializable("recibo",recibo);


    intent.putExtras(obj);
     startActivity(intent);

}
         }
     });


     btnSalir.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             finish();
         }
     });



    }


    public int  generarNumeroRecibo(){
       int numero=(int) (Math.random() * 1000);
       return numero;
    }
}
